<?php
    $menuItems = [
      [
        "name" => "Posts",
        "link" => "/posts",
      ],
        [
          "name" => "News",
          "link" => "/news",
        ]
    ];
?>
<nav class="navbar navbar-expand-sm navbar-dark bg-dark">
    <a class="navbar-brand" href="/">WebDevApp</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar"
            aria-controls="navbar" aria-expanded="false" aria-label="Toggle menu">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbar">
        <ul class="navbar-nav mr-auto">
            <?php foreach ($menuItems as $menuItem) { ?>
            <li class="nav-item active">
                <a class="nav-link" href="<?php echo $menuItem['link']; ?>">
                    <?php echo $menuItem['name']; ?>
                </a>
            </li>
            <?php } ?>
        </ul>
    </div>
    <div class="nav-item d-flex">
        <?php if (isset($user)) { ?>
            <?php if ($user->isAdmin()) { ?>
              <span class="navbar-text text-danger mr-3 d-inline-block">
                  Admin
              </span>
            <?php } ?>
          <span class="navbar-text text-white">
              <?php echo $user->getName(); ?>
          </span>
            <form action="/logout" method="post">
                <button type="submit" class="border-0 bg-transparent nav-link text-white-50">
                    Logout
                </button>
            </form>
        <?php } else { ?>
            <a href="/signin" class="nav-link text-white">
                Sign In
            </a>
        <?php } ?>
    </div>
</nav>
