<div class="col-12">
    <div class="col-md-12 mb-2">
        <?php echo 'Ви відвідували цю сторінку: ' . $countVisitPage . ' раз'; ?>
    </div>
    <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
        <div class="col p-4 d-flex flex-column position-static">
            <strong class="d-inline-block mb-2 text-primary"><?php echo $post->getGroup(); ?></strong>
            <h3 class="mb-0"><?php echo $post->getTitle(); ?></h3>
            <div class="mb-1 text-muted"><?php echo $post->getCreatedAt()->format('M d'); ?></div>
            <p class="card-text mb-auto"><?php echo $post->getDescription(); ?></p>
        </div>
        <div class="col-auto d-none d-lg-block">
            <img width="200" height="250" src="<?php echo $post->getImage(); ?>"
                 alt="<?php echo $post->getTitle(); ?>"/>
        </div>
    </div>
</div>
