<div class="col-md-6 mx-auto my-5">
    <h1 class="h3 mb-3 font-weight-normal text-center">Create new Post</h1>
    <form id="form-create-post" method="post" action="/admin/posts/create">
        <div class="form-group">
            <label for="title">Title</label>
            <input type="text" class="form-control" id="title" name="title" placeholder="Title" required>
        </div>
        <div class="form-group">
            <label for="description">Description</label>
            <textarea class="form-control" id="description" name="description" placeholder="Description"></textarea>
        </div>
        <div class="form-group">
            <label for="group">Group</label>
            <input type="text" class="form-control" id="group" name="group" placeholder="Group" required>
        </div>
        <div class="form-group">
            <label for="image">Image link</label>
            <input type="text" class="form-control" id="image" name="image" placeholder="Image link" required>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary mx-auto mt-3 px-4">Create</button>
        </div>
    </form>
</div>
