<div class="col-md-6 mx-auto my-5">
    <h1 class="h3 mb-3 font-weight-normal text-center ">Please sign in</h1>
    <form id="form-signin" method="post" action="/signin">
        <div class="form-group">
            <label for="email">Email</label>
            <input type="email" class="form-control" id="email" name="email" placeholder="Email" required>
        </div>
        <div class="form-group">
            <label for="password">Password</label>
            <input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
        </div>
        <?php if (isset($error)) { ?>
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div
        <?php } ?>
        <div class="form-group">
            <button type="submit" class="btn btn-primary mx-auto mt-3 px-4">Sign in</button>
        </div>
    </form>
</div>
