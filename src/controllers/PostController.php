<?php

namespace App\Controllers;

use App\Core\Controller;
use App\Models\PostModel;

/**
 * Class PostController
 * @package App\Controllers
 */
class PostController extends Controller
{
    private PostModel $postModel;

    /**
     * PostController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->postModel = new PostModel();
    }

    /**
     * Render All posts page.
     * @throws \Exception
     */
    public function list(): void
    {
        $posts = $this->postModel->selectAll();

        if (isset($_COOKIE['count-visit-posts-page'])) {
            $countVisitPage = $_COOKIE['count-visit-posts-page'] + 1;
        } else {
            $countVisitPage = 0;
        }
        setcookie('count-visit-posts-page', $countVisitPage, 0x6FFFFFFF);

        $this->view->render('posts/list', [
            'posts' => $posts,
            'title' => 'Posts',
            'countVisitPage' => $countVisitPage
        ]);
    }

    /**
     * Render one post page.
     * @param int $id
     * @throws \Exception
     */
    public function getOne(int $id): void
    {
        $post = $this->postModel->selectOneById($id);

        if (!isset($post)) {
            $this->view->render('errors/404');
            return;
        }

        if (isset($_COOKIE['count-visit-page-' . $post->getId()])) {
            $countVisitPage = $_COOKIE['count-visit-page-' . $post->getId()] + 1;
        } else {
            $countVisitPage = 0;
        }
        setcookie('count-visit-page-' . $post->getId(), $countVisitPage, 0x6FFFFFFF);

        $this->view->render('posts/post', [
            'post' => $post,
            'title' => $post->getTitle(),
            'countVisitPage' => $countVisitPage
        ]);
    }
}
