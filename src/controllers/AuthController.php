<?php

namespace App\Controllers;

use App\Core\Controller;
use App\Models\UserModel;
use App\Services\AuthService;

/**
 * Class AuthController
 * @package App\Controllers
 */
class AuthController extends Controller
{
    private UserModel $userModel;

    /**
     * AuthController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->userModel = new UserModel();
    }

    /**
     * Render Signin page.
     * @throws \Exception
     */
    public function index(): void
    {
        if (isset($this->user)) {
            $this->view->redirect('/');
            return;
        }

        $this->view->render('auth/signin');
    }

    /**
     * Authorizing user.
     * @throws \Exception
     */
    public function signIn(): void
    {
        $email = $_POST['email'];
        $password = $_POST['password'];

        $user = $this->userModel->selectOneByEmail($email);

        if (!$user) {
            $this->view->render('auth/signin', [
                'error' => 'User does not exist. Please check email.'
            ]);
            return;
        }

        if ($user->getPassword() !== md5($password)) {
            $this->view->render('auth/signin', [
                'error' => 'Incorrect password.'
            ]);
            return;
        }

        AuthService::authorizeUser($user);
        $this->view->redirect('/');
    }

    /**
     * Logout.
     */
    public function logout()
    {
        AuthService::unauthorizeUser();
        $this->view->redirect('/signin');
    }
}
