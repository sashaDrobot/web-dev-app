<?php

namespace App\Controllers;

use App\Core\Controller;
use App\Models\PostModel;

/**
 * Class AdminController
 * @package App\Controllers
 */
class AdminController extends Controller
{
    private PostModel $postModel;

    /**
     * PostController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->postModel = new PostModel();
    }

    /**
     * Show create post page.
     * @throws \Exception
     */
    public function showCreatePost(): void
    {
        if (!$this->user || !$this->user->isAdmin()) {
            $this->view->render('errors/403');
            return;
        }

        $this->view->render('admin/posts/create');
    }

    /**
     * Create new post.
     */
    public function createPost(): void
    {
        $title = $_POST['title'];
        $description = $_POST['description'];
        $group = $_POST['group'];
        $image = $_POST['image'];

        $this->postModel->insert($title, $description, $group, $image);
        $this->view->redirect('/posts');
    }

    /**
     * Remove post.
     * @param int $id
     * @throws \Exception
     */
    public function removePost(int $id): void
    {
        if (!$this->user || !$this->user->isAdmin()) {
            $this->view->render('errors/403');
            return;
        }

        $this->postModel->deleteOneById($id);
        $this->view->redirect('/');
    }
}
