<?php

namespace App\Core;

use App\Entities\User;
use App\Services\AuthService;

/**
 * Class Controller
 * @package App\Core
 */
class Controller
{
    protected View $view;
    protected ?User $user;

    /**
     * Controller constructor.
     */
    public function __construct()
    {
        $this->user = AuthService::getAuthorizedUser();
        $this->view = new View();
    }
}
