<?php

namespace App\Core;

use PDO;

/**
 * Class Model
 * @package App\Core
 */
class Model
{
    protected PDO $db;

    /**
     * Model constructor.
     */
    public function __construct()
    {
        $dbConfig = require __DIR__ . '/../config/db.php';
        $this->db = new PDO(
            $dbConfig['driver'] . ':host=' . $dbConfig['host'].';dbname=' . $dbConfig['dbname'] . ';charset=' . $dbConfig['charset'],
            $dbConfig['username'],
            $dbConfig['password']
        );
    }
}
