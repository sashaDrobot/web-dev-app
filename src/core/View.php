<?php

namespace App\Core;

use App\Services\AuthService;

/**
 * Class View
 * @package App\Core
 */
class View
{
    /**
     * Render template.
     * @param $viewName
     * @param array $vars
     * @throws \Exception
     */
    public function render($viewName, $vars = []): void
    {
        // Створюються змінні з іменами ключів массива $vars та відповідними значеннями
        extract($vars);
        $user = AuthService::getAuthorizedUser();
        // Формується шлях до файлу view
        $path = VIEWS_PATH . $viewName . '.php';

        // Якщо файл view не знайдено, то кидається виняток з повідомленням про помилку
        if (!file_exists($path)) {
            throw new \Exception('View not found.');
        }

        // Включення буферу виводу
        ob_start();
        // Підключення файлу view
        require_once $path;
        // В змінну $content записується значення з буферу виводу (подключений файл), буфер очищається та відключається
        $content = ob_get_clean();
        // Підключається layout
        require_once VIEWS_PATH . 'layout.php';
    }

    /**
     * Redirect to url.
     * @param string $url
     */
    public function redirect(string $url): void
    {
        header('location: ' . $url);
        exit;
    }
}
