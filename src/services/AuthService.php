<?php

namespace App\Services;

use App\Entities\User;

/**
 * Class AuthService
 * @package App\Services
 */
class AuthService
{
    public static function getAuthorizedUser(): ?User
    {
        return $_SESSION['user'] ?? null;
    }

    public static function authorizeUser(User $user): void
    {
        $_SESSION['user'] = $user;
    }

    public static function unauthorizeUser(): void
    {
        unset($_SESSION['user']);
    }
}
