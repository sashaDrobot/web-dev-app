<?php

namespace App\Models;

use App\Core\Model;
use App\Entities\Post;

/**
 * Class PostModel
 * @package App\Models
 */
class PostModel extends Model
{
    /**
     * Select all posts
     * @return array
     * @throws \Exception
     */
    public function selectAll(): array
    {
        $query = $this->db->query('SELECT * FROM posts;');
        $results = $query->fetchAll();

        $posts = [];
        foreach ($results as $result) {
            $posts[] = (new Post())
                ->setId($result['id'])
                ->setTitle($result['title'])
                ->setDescription($result['description'])
                ->setGroup($result['group'])
                ->setCreatedAt($result['created_at'])
                ->setImage($result['image']);
        }

        return $posts;
    }

    /**
     * Select post by Id
     * @param int $id
     * @return Post|null
     * @throws \Exception
     */
    public function selectOneById(int $id): ?Post
    {
        $query = $this->db->prepare('SELECT * FROM posts WHERE id = ?;');
        $query->execute([$id]);
        $result = $query->fetch();

        if (!$result) {
            return null;
        }

        return (new Post())
            ->setId($result['id'])
            ->setTitle($result['title'])
            ->setDescription($result['description'])
            ->setGroup($result['group'])
            ->setCreatedAt($result['created_at'])
            ->setImage($result['image']);
    }

    /**
     * @param string $title
     * @param string $description
     * @param string $group
     * @param string $image
     * @return bool
     */
    public function insert(string $title, string $description, string $group, string $image): bool
    {
        $query = $this->db->prepare('INSERT INTO posts VALUES (NULL, ?, ?, ?, ?, ?);');
        $query->execute([$title, $description, $group, $image, date('Y-m-d H:i:s')]);

        return true;
    }

    /**
     * @param int $id
     * @return bool
     */
    public function deleteOneById(int $id): bool
    {
        $query = $this->db->prepare('DELETE FROM posts WHERE id = ?;');
        $query->execute([$id]);

        return true;
    }
}
