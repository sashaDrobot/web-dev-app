<?php

namespace App\Models;

use App\Core\Model;
use App\Entities\User;

/**
 * Class UserModel
 * @package App\Models
 */
class UserModel extends Model
{
    /**
     * @param string $email
     * @return User|null
     */
    public function selectOneByEmail(string $email): ?User
    {
        $query = $this->db->prepare('SELECT * FROM users WHERE email = ?;');
        $query->execute([$email]);
        $result = $query->fetch();

        if (!$result) {
            return null;
        }

        return (new User())
            ->setId($result['id'])
            ->setName($result['name'])
            ->setEmail($result['email'])
            ->setPassword($result['password'])
            ->setIsAdmin($result['is_admin']);
    }
}
