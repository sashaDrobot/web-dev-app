<?php

defined("VIEWS_PATH")
    or define("VIEWS_PATH", realpath(dirname(__FILE__) . '/views') . '/');

ini_set('display_errors', 1);
error_reporting(E_ALL);
