<?php

// Підключається файл для автоматичного завантаження класів
require_once '../vendor/autoload.php';
// Підключається файл конфігуряцій
require_once '../src/config.php';

use App\Controllers\AdminController;
use App\Controllers\AuthController;
use App\Controllers\PostController;

session_start();

$path = parse_url($_SERVER['REQUEST_URI'])['path'];
$method = $_SERVER['REQUEST_METHOD'];

$adminController = new AdminController();
$authController = new AuthController();
$postController = new PostController();

switch (true) {
    case $path === '/signin' && $method === 'GET':
        $authController->index();
        break;
    case $path === '/signin' && $method === 'POST':
        $authController->signIn();
        break;
    case $path === '/logout' && $method === 'POST':
        $authController->logout();
        break;
    case in_array($path, ['/', '/posts']):
        $postController->list();
        break;
    case preg_match('~/posts/(\d+)~', $path, $matches):
        $id = (int) $matches[1];
        $postController->getOne($id);
        break;
    case $path === '/admin/posts/create' && $method === 'GET':
        $adminController->showCreatePost();
        break;
    case $path === '/admin/posts/create' && $method === 'POST':
        $adminController->createPost();
        break;
    case preg_match('~/admin/posts/delete/(\d+)~', $path, $matches):
        $id = (int) $matches[1];
        $adminController->removePost($id);
        break;
}
